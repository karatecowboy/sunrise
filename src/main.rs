
mod lexer;
use lexer::Lexer;
mod token;
//use token;
extern crate regex;

pub fn main(){
    let sample_code = "puts 'hello world'
    100.times do |t| puts t end".to_string();
    let tokenizer = Lexer::new();
    let tokens = tokenizer.tokenize(sample_code);
    for t in &tokens {
        println!("tokens is {:?}", t);
    }
}
