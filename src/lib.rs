extern crate regex;

mod lexer;
mod token;
use token::Token;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_takes_a_string() {
        let lexer = lexer::Lexer::new();
        let puts = String::from("puts 'hello, world'");
        lexer.tokenize(puts);
    }

    #[test]
    fn it_returns_a_vector() {
        let lexer = lexer::Lexer::new();
        let puts = String::from("puts 'hello, world'");
        let tokens  = lexer.tokenize(puts);
    }

    #[test]
    fn it_makes_a_token_for_puts_identifier() {
        let lexer = lexer::Lexer::new();
        let puts = String::from("puts 'hello, world'");
        let tokens = lexer.tokenize(puts);
        assert_ne!(tokens.len(),0);
        let puts = &tokens[0];
        assert_eq!(puts.string_value, "puts");
        assert_eq!(puts.token_type, "PUTS");
    }

    #[test]
    fn it_stashes_regular_identifiers(){
        let lexer = lexer::Lexer::new();
        let kick_method = String::from("kick 'hello, world'");
        let tokens = lexer.tokenize(kick_method);
        assert_ne!(tokens.len(),0);
        let kick_result = &tokens[0];
        assert_eq!(kick_result.string_value, "kick");
        assert_eq!(kick_result.token_type, "IDENTIFIER");
    }

    #[test]
    fn it_tokenizes_constants(){
        let lexer = lexer::Lexer::new();
        let sample_constant = String::from("COUNTRY = 'United States'");
        let tokens = lexer.tokenize(sample_constant);
        assert_ne!(tokens.len(),0);
        let kick_result = &tokens[0];
        assert_eq!(kick_result.string_value, "COUNTRY");
        assert_eq!(kick_result.token_type, "CONSTANT");
    }

    #[test]
    fn it_tokenizes_numbers(){
        let lexer = lexer::Lexer::new();
        let sample_number = String::from("3");
        let tokens = lexer.tokenize(sample_number);
        assert_ne!(tokens.len(),0);
        let number_result = &tokens[0];
        assert_eq!(number_result.number_value, 3.0);
        assert_eq!(number_result.token_type, "NUMBER");
        
        let sample_number = String::from("245.47");
        let tokens = lexer.tokenize(sample_number);
        let number_result = &tokens[0];
        assert_eq!(number_result.number_value, 245.47);
        assert_eq!(number_result.token_type, "NUMBER");

        let sample_numbers = String::from("24547 300");
        let tokens = lexer.tokenize(sample_numbers);
        let number_result = &tokens[0];
        assert_eq!(tokens.len(), 2);
        assert_eq!(number_result.number_value, 24547.00);
        assert_eq!(number_result.token_type, "NUMBER");

        let second_result = &tokens[1];
        assert_eq!(second_result.number_value, 300.0);
        assert_eq!(second_result.token_type, "NUMBER");
    }

}

