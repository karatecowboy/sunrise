use regex::Regex;
//use std;
use token::Token;


pub struct Lexer {
    keywords: Vec<&'static str>
}

impl Lexer {
    pub fn new() -> Lexer {
        Lexer {
            keywords: vec![
                "BEGIN",
                "END",
                "__ENCODING__",
                "__END__",
                "__FILE__",
                "__LINE__",
                "alias",
                "and",
                "begin",
                "break",
                "case",
                "class",
                "def",
                "defined?",
                "do",
                "else",
                "elsif",
                "end",
                "ensure",
                "false",
                "for",
                "if",
                "in",
                "module",
                "next",
                "nil",
                "not",
                "or",
                "puts",
                "redo",
                "rescue",
                "retry",
                "return",
                "self",
                "super",
                "then",
                "true",
                "undef",
                "unless",
                "until",
                "when",
                "while",
                "yield"
            ]
        }
    }
    pub fn tokenize(&self, code: String) -> Vec<Token> {
        let mut i: usize = 0;
        //let mut current_indent = 0;
        let mut tokens = Vec::new();
        //let indent_stack = [""];

        let keyword_regex: Regex = Regex::new(r"\A([a-z]\w*)").unwrap();
        let constant_regex: Regex = Regex::new(r"\A([A-Z]\w*)").unwrap();
        let number_regex: Regex = Regex::new(r"\A([\d]+[\.]{0,1}[\d]*\w*)").unwrap();
        let whitespace_regex : Regex = Regex::new(r"\A\w").unwrap();
        while i < code.len() {
            let chunk: &str = &code[i..(code.len())];
            if keyword_regex.is_match(&chunk) {
                let captures = keyword_regex.captures(&chunk);
                let identifier: String = captures.unwrap()[0].to_string();
                let identity_check: &str = &identifier[0..];
                if self.keywords.contains(&identity_check) {
                    let mut new_keyword: Token = Token::new();
                    let identifier_key: String = identifier.clone().to_uppercase();
                    new_keyword.token_type = identifier_key;
                    new_keyword.string_value = identifier.clone();
                    i = i + new_keyword.string_value.len();
                    tokens.push(new_keyword);
                } else {
                    let mut new_identifier: Token = Token::new();
                    new_identifier.token_type = "IDENTIFIER".to_string();
                    new_identifier.string_value = identifier.clone();
                    i = i + new_identifier.string_value.len();
                    tokens.push(new_identifier);
                }
            } else if constant_regex.is_match(chunk) {
                let captures = constant_regex.captures(&chunk);
                let constant: String = captures.unwrap()[0].to_string();
                let mut new_constant: Token = Token::new();
                new_constant.token_type ="CONSTANT".to_string();
                new_constant.string_value = constant.clone();
                i = i + new_constant.string_value.len();
                tokens.push(new_constant);
            } else if number_regex.is_match(chunk) {
                let captures = number_regex.captures(&chunk);
                let number: String = captures.unwrap()[0].to_string();
                i = i + number.len();
                let mut new_number: Token = Token::new();
                new_number.token_type = "NUMBER".to_string();
                let converted_number = number.parse::<f32>().unwrap();
                new_number.number_value = converted_number;
                tokens.push(new_number);
            } else if whitespace_regex.is_match(chunk) {
                i = i + 1;
            } else {
                i = i + 1;
            }
        }
        return tokens;
    }
}
