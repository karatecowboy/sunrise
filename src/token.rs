#[derive(Debug)]
pub struct Token {
    pub token_type: String,
    pub string_value: String,
    pub number_value: f32
}
impl Token {
    pub fn new() -> Token {
        Token { 
          token_type: "".to_string(),
          string_value: "".to_string(),
          number_value: 0.0
        }
    }
}



