extern crate getopts;
use getopts::Options;
use std::env;

fn usage(){
  println!("Usage: RubyWeb [options] file");
	println!("options: TDB");
}

struct Sunrise {
  id: i8
}
impl Sunrise {
  pub fn new() -> Sunrise {
	  Sunrise { id: 1 }
	}
	pub fn eval(&self) {
     println!("some ruby stuff -- dummy eval");
	}
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let mut opts = Options::new();
    let sunrise =  Sunrise::new();
 	sunrise.eval();
}
